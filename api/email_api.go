package api

import (
	"log"

//	. "bitbucket.org/SujitS/emailapi/structure"
	. "bitbucket.org/SujitS/emailapi/utils"

	"github.com/emersion/go-imap/client"
	"github.com/emersion/go-imap"
)
const (
	Address = "imap.gmail.com:993"
)

func FetchEmail(username string, password string) []string{
	c, err := client.DialTLS(Address, nil)
	PanicOnError(err)

	log.Println("Connected")

	// logout after the surrounding code executes
	defer c.Logout()

	loginError := c.Login(username, password)
	PanicOnError(loginError)

	// List MailBox
	mailboxes := make(chan *imap.MailboxInfo, 10)
	done := make(chan error, 1)
	go func () {
		done <- c.List("", "*", mailboxes)
	}()
	log.Println("Mailboxes:")
	for m := range mailboxes {
		log.Println("* " + m.Name)
	}

	err = <-done
	PanicOnError(err)

	// Select Inbox
	mbox, err := c.Select("INBOX", false)
	PanicOnError(err)

	// Get the last 4 email
	from := uint32(1)
	to := mbox.Messages
	if mbox.Messages > 3 {
		// We're using unsigned integers here, only substract if the result is > 0
		from = mbox.Messages - 3
	}
	seqset := new(imap.SeqSet)
	seqset.AddRange(from, to)

	eMessages := []string{}
	messages := make(chan *imap.Message, 10)
	done = make(chan error, 1)
	go func() {
		done <- c.Fetch(seqset, []string{imap.EnvelopeMsgAttr}, messages)
	}()

	log.Println("Last 4 messages:")
	for msg := range messages {
		eMessages = append(eMessages, msg.Envelope.Subject)
	}

	err = <-done
	PanicOnError(err)
	log.Println("Done!")

	return eMessages
}
