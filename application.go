package main

import (
	"net/http"
	"log"

	. "bitbucket.org/SujitS/emailapi/handler"
)

// main application function
func main() {
	port := ":3021"

	mux := http.NewServeMux()
	mux.HandleFunc("/email", RequestHandler)
	log.Fatal(http.ListenAndServe(port, mux))
}
