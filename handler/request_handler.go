package handler

import (
	"net/http"
	"encoding/json"

	. "bitbucket.org/SujitS/emailapi/structure"
	. "bitbucket.org/SujitS/emailapi/utils"
	api "bitbucket.org/SujitS/emailapi/api"
)

func RequestHandler(w http.ResponseWriter, req *http.Request) {
	if req.Method == "POST" {
		request := Request{} // initialize empty Request
		err := json.NewDecoder(req.Body).Decode(&request)
		PanicOnError(err)

		response := api.FetchEmail(request.Username, request.Password)
		// response.Message = "Hello Go server is up and running ..."
		// responseJson, err := json.Marshal(response)
		// PanicOnError(err)

		w.Header().Set("Server", "Email API Service")
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		// w.Write(responseJson)
		json.NewEncoder(w).Encode(response)
	} else {
		http.Error(w, "Invalid Request Method", http.StatusMethodNotAllowed)
	}
}
